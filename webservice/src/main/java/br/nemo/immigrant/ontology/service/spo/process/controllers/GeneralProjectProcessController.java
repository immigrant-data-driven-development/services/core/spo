package br.nemo.immigrant.ontology.service.spo.process.controllers;

import br.nemo.immigrant.ontology.entity.spo.process.models.GeneralProjectProcess;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.GeneralProjectProcessRepository;
import br.nemo.immigrant.ontology.service.spo.process.records.GeneralProjectProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class GeneralProjectProcessController  {

  @Autowired
  GeneralProjectProcessRepository repository;

  @QueryMapping
  public List<GeneralProjectProcess> findAllGeneralProjectProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public GeneralProjectProcess findByIDGeneralProjectProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public GeneralProjectProcess createGeneralProjectProcess(@Argument GeneralProjectProcessInput input) {
    GeneralProjectProcess instance = GeneralProjectProcess.builder().name(input.name()).
                                                                     description(input.description()).
                                                                     startDate(input.startDate()).
                                                                     endDate(input.endDate()).
                                                                     internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public GeneralProjectProcess updateGeneralProjectProcess(@Argument Long id, @Argument GeneralProjectProcessInput input) {
    GeneralProjectProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("GeneralProjectProcess not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteGeneralProjectProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
