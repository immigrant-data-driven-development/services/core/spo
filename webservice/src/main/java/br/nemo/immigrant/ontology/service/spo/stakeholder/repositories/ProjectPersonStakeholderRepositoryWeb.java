package br.nemo.immigrant.ontology.service.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectPersonStakeholderRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "projectpersonstakeholder", path = "projectpersonstakeholder")
public interface ProjectPersonStakeholderRepositoryWeb extends ProjectPersonStakeholderRepository {

}
