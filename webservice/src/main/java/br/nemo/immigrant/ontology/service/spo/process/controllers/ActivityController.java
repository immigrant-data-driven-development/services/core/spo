package br.nemo.immigrant.ontology.service.spo.process.controllers;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityRepository;
import br.nemo.immigrant.ontology.service.spo.process.records.ActivityInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ActivityController  {

  @Autowired
  ActivityRepository repository;

  @QueryMapping
  public List<Activity> findAllActivitys() {
    return repository.findAll();
  }

  @QueryMapping
  public Activity findByIDActivity(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Activity createActivity(@Argument ActivityInput input) {
    Activity instance = Activity.builder().name(input.name()).
                                           description(input.description()).
                                           startDate(input.startDate()).
                                           endDate(input.endDate()).
                                           internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Activity updateActivity(@Argument Long id, @Argument ActivityInput input) {
    Activity instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Activity not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteActivity(@Argument Long id) {
    repository.deleteById(id);
  }

}
