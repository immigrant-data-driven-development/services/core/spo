package br.nemo.immigrant.ontology.service.spo.project.repositories;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "softwareproject", path = "softwareproject")
public interface SoftwareProjectRepositoryWeb extends SoftwareProjectRepository {

}
