package br.nemo.immigrant.ontology.service.spo.artifact.controllers;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactRepository;
import br.nemo.immigrant.ontology.service.spo.artifact.records.ArtifactInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ArtifactController  {

  @Autowired
  ArtifactRepository repository;

  @QueryMapping
  public List<Artifact> findAllArtifacts() {
    return repository.findAll();
  }

  @QueryMapping
  public Artifact findByIDArtifact(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Artifact createArtifact(@Argument ArtifactInput input) {
    Artifact instance = Artifact.builder().name(input.name()).
                                           description(input.description()).
                                           startDate(input.startDate()).
                                           endDate(input.endDate()).
                                           internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Artifact updateArtifact(@Argument Long id, @Argument ArtifactInput input) {
    Artifact instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Artifact not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteArtifact(@Argument Long id) {
    repository.deleteById(id);
  }

}
