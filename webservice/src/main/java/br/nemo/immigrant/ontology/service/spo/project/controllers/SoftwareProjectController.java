package br.nemo.immigrant.ontology.service.spo.project.controllers;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.entity.spo.project.repositories.SoftwareProjectRepository;
import br.nemo.immigrant.ontology.service.spo.project.records.SoftwareProjectInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SoftwareProjectController  {

  @Autowired
  SoftwareProjectRepository repository;

  @QueryMapping
  public List<SoftwareProject> findAllSoftwareProjects() {
    return repository.findAll();
  }

  @QueryMapping
  public SoftwareProject findByIDSoftwareProject(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public SoftwareProject createSoftwareProject(@Argument SoftwareProjectInput input) {
    SoftwareProject instance = SoftwareProject.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public SoftwareProject updateSoftwareProject(@Argument Long id, @Argument SoftwareProjectInput input) {
    SoftwareProject instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("SoftwareProject not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteSoftwareProject(@Argument Long id) {
    repository.deleteById(id);
  }

}
