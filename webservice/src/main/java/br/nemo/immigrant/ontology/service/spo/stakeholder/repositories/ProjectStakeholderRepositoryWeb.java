package br.nemo.immigrant.ontology.service.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "projectstakeholder", path = "projectstakeholder")
public interface ProjectStakeholderRepositoryWeb extends ProjectStakeholderRepository {

}
