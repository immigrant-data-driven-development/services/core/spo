package br.nemo.immigrant.ontology.service.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectTeamStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectTeamStakeholderRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "projectteamstakeholder", path = "projectteamstakeholder")
public interface ProjectTeamStakeholderRepositoryWeb extends ProjectTeamStakeholderRepository {

}
