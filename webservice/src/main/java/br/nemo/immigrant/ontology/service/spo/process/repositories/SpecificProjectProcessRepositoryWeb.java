package br.nemo.immigrant.ontology.service.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "specificprojectprocess", path = "specificprojectprocess")
public interface SpecificProjectProcessRepositoryWeb extends SpecificProjectProcessRepository {

}
