package br.nemo.immigrant.ontology.service.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.GeneralProjectProcess;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.GeneralProjectProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "generalprojectprocess", path = "generalprojectprocess")
public interface GeneralProjectProcessRepositoryWeb extends GeneralProjectProcessRepository {

}
