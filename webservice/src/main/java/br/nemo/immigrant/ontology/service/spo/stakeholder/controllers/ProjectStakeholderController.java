package br.nemo.immigrant.ontology.service.spo.stakeholder.controllers;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectStakeholderRepository;
import br.nemo.immigrant.ontology.service.spo.stakeholder.records.ProjectStakeholderInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProjectStakeholderController  {

  @Autowired
  ProjectStakeholderRepository repository;

  @QueryMapping
  public List<ProjectStakeholder> findAllProjectStakeholders() {
    return repository.findAll();
  }

  @QueryMapping
  public ProjectStakeholder findByIDProjectStakeholder(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ProjectStakeholder createProjectStakeholder(@Argument ProjectStakeholderInput input) {
    ProjectStakeholder instance = ProjectStakeholder.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ProjectStakeholder updateProjectStakeholder(@Argument Long id, @Argument ProjectStakeholderInput input) {
    ProjectStakeholder instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ProjectStakeholder not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteProjectStakeholder(@Argument Long id) {
    repository.deleteById(id);
  }

}
