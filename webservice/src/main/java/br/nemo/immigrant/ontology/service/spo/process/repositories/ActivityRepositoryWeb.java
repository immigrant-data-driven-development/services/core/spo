package br.nemo.immigrant.ontology.service.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.ActivityRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "activity", path = "activity")
public interface ActivityRepositoryWeb extends ActivityRepository {

}
