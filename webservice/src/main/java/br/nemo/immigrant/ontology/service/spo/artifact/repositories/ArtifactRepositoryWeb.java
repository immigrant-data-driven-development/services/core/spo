package br.nemo.immigrant.ontology.service.spo.artifact.repositories;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import br.nemo.immigrant.ontology.entity.spo.artifact.repositories.ArtifactRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "artifact", path = "artifact")
public interface ArtifactRepositoryWeb extends ArtifactRepository {

}
