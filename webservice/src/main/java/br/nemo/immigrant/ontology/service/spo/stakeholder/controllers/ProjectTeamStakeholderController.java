package br.nemo.immigrant.ontology.service.spo.stakeholder.controllers;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectTeamStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectTeamStakeholderRepository;
import br.nemo.immigrant.ontology.service.spo.stakeholder.records.ProjectTeamStakeholderInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProjectTeamStakeholderController  {

  @Autowired
  ProjectTeamStakeholderRepository repository;

  @QueryMapping
  public List<ProjectTeamStakeholder> findAllProjectTeamStakeholders() {
    return repository.findAll();
  }

  @QueryMapping
  public ProjectTeamStakeholder findByIDProjectTeamStakeholder(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ProjectTeamStakeholder createProjectTeamStakeholder(@Argument ProjectTeamStakeholderInput input) {
    ProjectTeamStakeholder instance = ProjectTeamStakeholder.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ProjectTeamStakeholder updateProjectTeamStakeholder(@Argument Long id, @Argument ProjectTeamStakeholderInput input) {
    ProjectTeamStakeholder instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ProjectTeamStakeholder not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteProjectTeamStakeholder(@Argument Long id) {
    repository.deleteById(id);
  }

}
