package br.nemo.immigrant.ontology.service.spo.artifact.records;
import java.time.LocalDate;
public record ArtifactInput( String name,String description,LocalDate startDate,LocalDate endDate,String externalId,String internalId ) {
}
