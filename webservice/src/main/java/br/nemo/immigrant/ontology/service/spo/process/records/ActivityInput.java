package br.nemo.immigrant.ontology.service.spo.process.records;
import java.time.LocalDate;
public record ActivityInput( String name,String description,LocalDate startDate,LocalDate endDate,String externalId,String internalId ) {
}
