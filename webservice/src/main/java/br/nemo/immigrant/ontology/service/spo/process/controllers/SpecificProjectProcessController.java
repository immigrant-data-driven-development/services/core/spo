package br.nemo.immigrant.ontology.service.spo.process.controllers;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;
import br.nemo.immigrant.ontology.entity.spo.process.repositories.SpecificProjectProcessRepository;
import br.nemo.immigrant.ontology.service.spo.process.records.SpecificProjectProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SpecificProjectProcessController  {

  @Autowired
  SpecificProjectProcessRepository repository;

  @QueryMapping
  public List<SpecificProjectProcess> findAllSpecificProjectProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public SpecificProjectProcess findByIDSpecificProjectProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public SpecificProjectProcess createSpecificProjectProcess(@Argument SpecificProjectProcessInput input) {
    SpecificProjectProcess instance = SpecificProjectProcess.builder().name(input.name()).
                                                                       description(input.description()).
                                                                       startDate(input.startDate()).
                                                                       endDate(input.endDate()).
                                                                       internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public SpecificProjectProcess updateSpecificProjectProcess(@Argument Long id, @Argument SpecificProjectProcessInput input) {
    SpecificProjectProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("SpecificProjectProcess not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteSpecificProjectProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
