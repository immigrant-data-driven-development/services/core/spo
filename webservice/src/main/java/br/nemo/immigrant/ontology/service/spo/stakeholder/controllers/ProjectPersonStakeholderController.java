package br.nemo.immigrant.ontology.service.spo.stakeholder.controllers;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories.ProjectPersonStakeholderRepository;
import br.nemo.immigrant.ontology.service.spo.stakeholder.records.ProjectPersonStakeholderInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProjectPersonStakeholderController  {

  @Autowired
  ProjectPersonStakeholderRepository repository;

  @QueryMapping
  public List<ProjectPersonStakeholder> findAllProjectPersonStakeholders() {
    return repository.findAll();
  }

  @QueryMapping
  public ProjectPersonStakeholder findByIDProjectPersonStakeholder(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ProjectPersonStakeholder createProjectPersonStakeholder(@Argument ProjectPersonStakeholderInput input) {
    ProjectPersonStakeholder instance = ProjectPersonStakeholder.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ProjectPersonStakeholder updateProjectPersonStakeholder(@Argument Long id, @Argument ProjectPersonStakeholderInput input) {
    ProjectPersonStakeholder instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ProjectPersonStakeholder not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteProjectPersonStakeholder(@Argument Long id) {
    repository.deleteById(id);
  }

}
