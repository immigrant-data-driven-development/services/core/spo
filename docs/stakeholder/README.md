# 📕Documentation: Stakeholder

Stakeholders refer to people, teams or organizations acting in or performing process activities.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **ProjectStakeholder** : Stakeholder interested in a particular Project.
* **ProjectTeamStakeholder** : A Team that is a Stakeholder interested in a particular Project.
* **ProjectPersonStakeholder** : A Person that is a Stakeholder interested in a particular Project.
