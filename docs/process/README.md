# 📕Documentation: Process

The Performed Process sub-ontology deals with Performed Processes and Activities, and how they are decomposed.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **GeneralProjectProcess** : Refers to the whole intented or performed process defined for a project.
* **SpecificProjectProcess** : Refers a defined and intended or performed process with a specific purpose for a project.
* **Activity** : An intended or performed activity in a process.
