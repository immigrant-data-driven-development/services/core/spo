# 📕Documentation: Artifact

Artifacts represent different types of objects produced and used in process activities.

## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **Artifact** : Object intentionally made to serve a given purpose in the context of a software Project or Organization.
