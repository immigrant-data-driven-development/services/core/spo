# Software Process Ontology Webservice
## 🚀 Goal
The Software Process Ontology (SPO) aims at establishing a common conceptualization on the Software Process domain, including processes, activities, resources, people, artifacts and procedures. As a core ontology, SPO provides the general concepts for software processes, to be specialized and reused in domain-specific ontologies.

## 📕Documentation

The Documentation can be found [here](./docs/README.md)

## ⚙ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack 
 1. Spring Boot 3.0
 2. Spring Data Rest
 3. Spring GraphQL

## 🔧 Install

1. Create a database with name sro with **CREATE DATABASE sro**.
2. Run the command to start the webservice and create table of database.

```bash
    mvn Spring-boot:run 
```

## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash

curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-spo.json

```

To delete, uses:

```bash

curl -i -X DELETE localhost:8083/connectors/spo-connector/

```

## 🔧 Usage

* Access [http://localhost:8082](http://localhost:8082) to see Swagger.

* Access [http://localhost:8082/grapiql](http://localhost:8082/grapiql) Graphql.


## ✒ Team

* **[Paulo Sérgio dos Santos Júnior](paulosjunior@gmail.com)**

## 📕 Literature

* **[Article about SEON: A Software Engineering Ontology Network](https://nemo.inf.ufes.br/wp-content/uploads/2016/10/SEON_A-Software-Engineering-Ontology-Network-Ruy-et-al.-2016.pdf)**
* **[SEON Website](https://dev.nemo.inf.ufes.br/seon/)**
