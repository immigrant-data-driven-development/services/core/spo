
ALTER TABLE public.softwareproject  REPLICA IDENTITY FULL;
ALTER TABLE public.generalprojectprocess  REPLICA IDENTITY FULL;
ALTER TABLE public.specificprojectprocess  REPLICA IDENTITY FULL;
ALTER TABLE public.activity  REPLICA IDENTITY FULL;
ALTER TABLE public.projectstakeholder  REPLICA IDENTITY FULL;
ALTER TABLE public.projectteamstakeholder  REPLICA IDENTITY FULL;
ALTER TABLE public.projectpersonstakeholder  REPLICA IDENTITY FULL;
ALTER TABLE public.artifact  REPLICA IDENTITY FULL;
