package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ProjectStakeholderRepository extends PagingAndSortingRepository<ProjectStakeholder, Long>, ListCrudRepository<ProjectStakeholder, Long> {

}
