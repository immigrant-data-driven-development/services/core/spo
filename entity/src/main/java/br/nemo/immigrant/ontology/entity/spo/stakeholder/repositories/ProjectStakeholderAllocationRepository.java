package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderAllocation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import org.springframework.data.repository.query.Param;

public interface ProjectStakeholderAllocationRepository extends PagingAndSortingRepository<ProjectStakeholderAllocation, Long>, ListCrudRepository<ProjectStakeholderAllocation, Long> {

    Optional<ProjectStakeholderAllocation> findFirstByProjectstakeholderAndActivity(@Param("projectStakeholder") ProjectStakeholder projectStakeholder, @Param("activity")  Activity activity);

    Optional<ProjectStakeholderAllocation> findByInternalId(@Param("internalId")  String internalId);

    boolean existsByInternalId(@Param("internalId") String internalId);

}
