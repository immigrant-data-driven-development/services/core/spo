package br.nemo.immigrant.ontology.entity.spo.process.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "activity_event")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ActivityEvent  implements Serializable {

    @Id
    protected @GeneratedValue (strategy=GenerationType.IDENTITY)
    Long id;


    @ManyToOne (cascade = CascadeType.MERGE)
    @JoinColumn(name = "activity")
    Activity activity;

    private LocalDateTime eventDate;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private StateType statetype = StateType.INTENDED;


    @Builder.Default
    @Enumerated(EnumType.STRING)
    private SuccessType successtype = SuccessType.UNDEFINED;

    @Column(unique=true)
    @Builder.Default
    private String internalId = UUID.randomUUID().toString();

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

      ActivityEvent elem = (ActivityEvent) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Activity {" +
         "id="+this.id+


          ", statetype='"+this.statetype+"'"+
          ", successtype='"+this.successtype+"'"+
      '}';
  }
}
