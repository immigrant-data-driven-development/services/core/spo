package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ActivityRepository extends JpaRepository<Activity, Long>, PagingAndSortingRepository<Activity, Long>, ListCrudRepository<Activity, Long> {

}
