package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;
import  br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import  br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderActivity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.query.Param;
public interface ProjectStakeholderActivityRepository extends PagingAndSortingRepository<ProjectStakeholderActivity, Long>, ListCrudRepository<ProjectStakeholderActivity, Long> {

    public Boolean existsByProjectstakeholderAndActivity(@Param("projectstakeholder") ProjectStakeholder projectstakeholder,@Param("activity") Activity activity);

}
