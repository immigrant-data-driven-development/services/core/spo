package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderSoftwareProject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ProjectStakeholderSoftwareProjectRepository extends PagingAndSortingRepository<ProjectStakeholderSoftwareProject, Long>, ListCrudRepository<ProjectStakeholderSoftwareProject, Long> {

}
