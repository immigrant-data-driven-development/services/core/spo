package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;

import  br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
public interface SpecificProjectProcessActivityRepository extends JpaRepository<SpecificProjectProcessActivity, Long>, PagingAndSortingRepository<SpecificProjectProcessActivity, Long>, ListCrudRepository<SpecificProjectProcessActivity, Long> {

    public Boolean existsBySpecificprojectprocessAndActivity(@Param("specificprojectprocess")  SpecificProjectProcess specificprojectprocess, @Param("activity")  Activity activity );

}
