package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
public interface SpecificProjectProcessArtifactRepository extends JpaRepository<SpecificProjectProcessArtifact, Long>, PagingAndSortingRepository<SpecificProjectProcessArtifact, Long>, ListCrudRepository<SpecificProjectProcessArtifact, Long> {

}