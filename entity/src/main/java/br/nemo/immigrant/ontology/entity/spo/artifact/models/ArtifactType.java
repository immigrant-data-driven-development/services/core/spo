package br.nemo.immigrant.ontology.entity.spo.artifact.models;

public enum ArtifactType {
    SOFTWAREPRODUCT,
    SOFTWAREITEM,
    INFORMATIONITEM,
    DOCUMENT
}

