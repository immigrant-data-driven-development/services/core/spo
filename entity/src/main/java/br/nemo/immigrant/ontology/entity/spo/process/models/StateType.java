package br.nemo.immigrant.ontology.entity.spo.process.models;

public enum StateType {
    PERFORMED,
    INTENDED
}