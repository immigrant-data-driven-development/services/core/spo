package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.GeneralProjectProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
public interface GeneralProjectProcessRepository extends JpaRepository<GeneralProjectProcess, Long>,PagingAndSortingRepository<GeneralProjectProcess, Long>, ListCrudRepository<GeneralProjectProcess, Long> {

}
