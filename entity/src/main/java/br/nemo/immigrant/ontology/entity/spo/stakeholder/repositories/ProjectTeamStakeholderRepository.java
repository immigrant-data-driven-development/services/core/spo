package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectTeamStakeholder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ProjectTeamStakeholderRepository extends PagingAndSortingRepository<ProjectTeamStakeholder, Long>, ListCrudRepository<ProjectTeamStakeholder, Long> {

}
