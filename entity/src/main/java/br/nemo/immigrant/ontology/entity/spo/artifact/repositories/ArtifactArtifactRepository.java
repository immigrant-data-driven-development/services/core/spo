    package br.nemo.immigrant.ontology.entity.spo.artifact.repositories;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.ArtifactArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
public interface ArtifactArtifactRepository extends JpaRepository<ArtifactArtifact, Long>, PagingAndSortingRepository<ArtifactArtifact, Long>, ListCrudRepository<ArtifactArtifact, Long> {

    public Boolean existsByArtifactfromAndArtifactto(@Param("artifactfrom") Artifact artifactfrom, @Param("artifactto") Artifact artifactto );

    ArtifactArtifact findByArtifactfromApplicationsExternalIdAndArtifacttoName(@Param("externalId") String externalId,@Param("name") String name);

}
