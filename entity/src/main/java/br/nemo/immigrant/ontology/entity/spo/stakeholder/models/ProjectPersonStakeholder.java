package br.nemo.immigrant.ontology.entity.spo.stakeholder.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ProjectPersonStakeholder extends ProjectStakeholder implements Serializable {

  @ManyToOne
  @JoinColumn(name = "person_id", referencedColumnName = "id",  nullable = true, unique = false)
  private Person person;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ProjectPersonStakeholder elem = (ProjectPersonStakeholder) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ProjectPersonStakeholder {" +
         "id="+this.id+



      '}';
  }
}
