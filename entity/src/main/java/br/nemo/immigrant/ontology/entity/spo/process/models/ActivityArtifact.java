package br.nemo.immigrant.ontology.entity.spo.process.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import  br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "activity_artifact")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

public  class ActivityArtifact  implements Serializable {


   @Id
   protected @GeneratedValue (strategy=GenerationType.IDENTITY)
   Long id;

   @ManyToOne
   @JoinColumn(name = "artifact_id")
   private Artifact artifact;

  @ManyToOne
  @JoinColumn(name = "activity_id")
  private Activity activity;

  @Builder.Default
  @Enumerated(EnumType.STRING)
  private ActionArtifactType actionartifacttype = ActionArtifactType.CREATED;

  @Column(unique=true)
  @Builder.Default
  private String internalId = UUID.randomUUID().toString();

  private LocalDateTime eventDate;
  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Activity elem = (Activity) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Activity {" +
         "id="+this.id+

         
      '}';
  }
}
