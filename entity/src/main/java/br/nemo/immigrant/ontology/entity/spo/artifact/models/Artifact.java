package br.nemo.immigrant.ontology.entity.spo.artifact.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessArtifact;
import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "artifact")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Artifact extends CommonConcept implements Serializable {


  @OneToMany(mappedBy = "artifact")
  Set<ProjectStakeholderArtifact> projectstakeholderartifact;

  @OneToMany(mappedBy = "artifact")
  Set<SpecificProjectProcessArtifact> specificprojectprocessartifact;


  @Builder.Default
  @Enumerated(EnumType.STRING)
  private ArtifactType artifacttype = ArtifactType.SOFTWAREPRODUCT;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Artifact elem = (Artifact) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Artifact {" +
         "id="+this.id+

          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", startDate='"+this.startDate+"'"+
          ", endDate='"+this.endDate+"'"+
           ", internalId='"+this.internalId+"'"+
          ", artifacttype='"+this.artifacttype+"'"+
      '}';
  }
}
