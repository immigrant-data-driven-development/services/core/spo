package br.nemo.immigrant.ontology.entity.spo.process.models;

public enum ActionArtifactType {
    CREATED,
    USAGE,
    DELETED,
    UPDATED

}