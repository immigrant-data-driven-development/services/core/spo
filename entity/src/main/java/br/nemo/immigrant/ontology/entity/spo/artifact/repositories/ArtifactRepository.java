package br.nemo.immigrant.ontology.entity.spo.artifact.repositories;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ArtifactRepository extends JpaRepository<Artifact, Long>, PagingAndSortingRepository<Artifact, Long>, ListCrudRepository<Artifact, Long> {

    Optional<IDProjection> findByApplicationsExternalId(@Param("externalId") String externalId);

    Optional<IDProjection> findByInternalId(@Param("internalId") String internalId);

    Boolean existsByInternalId(@Param("internalId") String internalId);
}
