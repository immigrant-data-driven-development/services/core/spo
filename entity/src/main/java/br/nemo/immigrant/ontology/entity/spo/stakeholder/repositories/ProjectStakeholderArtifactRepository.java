package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholder;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.query.Param;
public interface ProjectStakeholderArtifactRepository extends PagingAndSortingRepository<ProjectStakeholderArtifact, Long>, ListCrudRepository<ProjectStakeholderArtifact, Long> {

    Boolean existsByProjectstakeholderAndArtifact(@Param("projectstakeholder") ProjectStakeholder projectstakeholder, @Param("artifact") Artifact artifact);

}
