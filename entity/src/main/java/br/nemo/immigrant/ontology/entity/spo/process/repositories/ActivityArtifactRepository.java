package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
import  br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ActivityArtifactRepository extends JpaRepository<ActivityArtifact, Long>,PagingAndSortingRepository<ActivityArtifact, Long>, ListCrudRepository<ActivityArtifact, Long> {

    public Boolean existsByArtifactAndActivity(@Param("artifact")  Artifact artifact, @Param("activity")  Activity activity);

}
