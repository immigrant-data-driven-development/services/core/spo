
package br.nemo.immigrant.ontology.entity.spo.stakeholder.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import  br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "projectStakeholder_activity_allocation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ProjectStakeholderAllocation  implements Serializable {


   @Id
   protected @GeneratedValue (strategy=GenerationType.IDENTITY)
   Long id;
   
   @ManyToOne
   @JoinColumn(name = "projectstakeholder_id",nullable = true, unique = false)
   ProjectStakeholder projectstakeholder;

   @ManyToOne
   @JoinColumn(name = "activity_id", nullable = true, unique = false)
   Activity activity;

    private Integer plannedDuration;

    @Column(unique=true)
    @Builder.Default
    private String internalId = UUID.randomUUID().toString();

    private LocalDateTime plannedStartDate;
    private LocalDateTime plannedEventDate;

   @Builder.Default
   @Enumerated(EnumType.STRING)
   private Participation participation = Participation.PARTICIPATESIN;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ProjectStakeholderActivity elem = (ProjectStakeholderActivity) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ProjectStakeholderActivity {" +
         "id="+this.id+

         
      '}';
  }
}
