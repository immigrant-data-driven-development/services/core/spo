package br.nemo.immigrant.ontology.entity.spo.process.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;


import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "specificprojectprocess_specificprojectprocess")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class SpecificProjectProcessSpecificProjectProcess implements Serializable {


  @Id
   protected @GeneratedValue (strategy=GenerationType.IDENTITY)
   Long id;
   
   @ManyToOne
   @JoinColumn(name = "specificprojectprocess_from_id")
   SpecificProjectProcess specificprojectprocessfrom;

   @ManyToOne
   @JoinColumn(name = "specificprojectprocess_to_id")
   SpecificProjectProcess specificprojectprocessto;

   @Column(unique=true)
    @Builder.Default
    private String internalId = UUID.randomUUID().toString();
   private LocalDateTime eventDate;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;
      SpecificProjectProcessSpecificProjectProcess elem = (SpecificProjectProcessSpecificProjectProcess) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Artifact {" +
         "id="+this.id+

         
      '}';
  }
}
