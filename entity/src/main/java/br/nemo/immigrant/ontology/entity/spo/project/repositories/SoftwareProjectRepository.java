package br.nemo.immigrant.ontology.entity.spo.project.repositories;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface SoftwareProjectRepository extends JpaRepository<SoftwareProject, Long>, PagingAndSortingRepository<SoftwareProject, Long>, ListCrudRepository<SoftwareProject, Long> {
    Optional<IDProjection> findByApplicationsExternalId(@Param("externalId") String externalId);

    Optional<IDProjection> findByInternalId(@Param("internalId") String internalId);

    Boolean existsByInternalId(@Param("internalId") String internalId);
}
