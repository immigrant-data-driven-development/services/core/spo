package br.nemo.immigrant.ontology.entity.spo.process.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;

import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "generalprojectprocess")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class GeneralProjectProcess extends CommonConcept implements Serializable {

  @OneToOne
  @JoinColumn(name = "softwareproject_id", referencedColumnName = "id")
  private SoftwareProject softwareproject;


  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "generalprojectprocess")
  @Builder.Default
  Set<SpecificProjectProcess> specificprojectprocesss = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "generalprojectprocess_id")
  private GeneralProjectProcess generalprojectprocess;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "generalprojectprocess")
  @Builder.Default
  Set<GeneralProjectProcess> generalprojectprocesss = new HashSet<>();

  @Builder.Default
  @Enumerated(EnumType.STRING)
  private StateType statetype = StateType.PERFORMED;
  @Builder.Default
  @Enumerated(EnumType.STRING)
  private SuccessType successtype = SuccessType.SUCCESSS;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        GeneralProjectProcess elem = (GeneralProjectProcess) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "GeneralProjectProcess {" +
         "id="+this.id+

          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", startDate='"+this.startDate+"'"+
          ", endDate='"+this.endDate+"'"+
          ", internalId='"+this.internalId+"'"+
          ", statetype='"+this.statetype+"'"+
          ", successtype='"+this.successtype+"'"+
      '}';
  }
}
