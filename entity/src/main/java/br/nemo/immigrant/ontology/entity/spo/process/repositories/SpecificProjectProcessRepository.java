package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface SpecificProjectProcessRepository extends JpaRepository<SpecificProjectProcess, Long>, PagingAndSortingRepository<SpecificProjectProcess, Long>, ListCrudRepository<SpecificProjectProcess, Long> {

    Optional<IDProjection> findByGeneralprojectprocessSoftwareprojectApplicationsExternalId(@Param("externalId")  String externalId);
}
