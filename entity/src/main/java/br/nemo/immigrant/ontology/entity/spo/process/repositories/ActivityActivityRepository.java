package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

import  br.nemo.immigrant.ontology.entity.spo.process.models.ActivityActivity;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ActivityActivityRepository extends JpaRepository<ActivityActivity, Long>, PagingAndSortingRepository<ActivityActivity, Long>, ListCrudRepository<ActivityActivity, Long> {



}
