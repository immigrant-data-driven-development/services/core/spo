package br.nemo.immigrant.ontology.entity.spo.artifact.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "artifact_artifact")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ArtifactArtifact implements Serializable {


  @Id
   protected @GeneratedValue (strategy=GenerationType.IDENTITY)
   Long id;
   
   @ManyToOne
   @JoinColumn(name = "artifact_from_id")
   private Artifact artifactfrom;

   @ManyToOne
   @JoinColumn(name = "artifact_to_id")
   private Artifact artifactto;

   private LocalDateTime eventDate;
    @Column(unique=true)
    @Builder.Default
    private String internalId = UUID.randomUUID().toString();

   @Builder.Default
   @Enumerated(EnumType.STRING)
   private ArtifactRelation artifactrelation = ArtifactRelation.COMPOSED;

 
  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Artifact elem = (Artifact) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Artifact {" +
         "id="+this.id+

         
      '}';
  }
}
