package br.nemo.immigrant.ontology.entity.spo.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectPersonStakeholder;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import java.util.List;
import br.nemo.immigrant.ontology.entity.base.projections.IDOnlyProjection;
import org.springframework.data.repository.query.Param;

public interface ProjectPersonStakeholderRepository extends PagingAndSortingRepository<ProjectPersonStakeholder, Long>, ListCrudRepository<ProjectPersonStakeholder, Long> {
    
    Optional<IDOnlyProjection> findFirstByPersonApplicationsExternalIdAndProjectstakeholdersoftwareprojectSoftwareprojectName(@Param("personExternalId") String personExternalId,@Param("projectName") String projectName);

    Boolean existsByPersonEmailAndProjectstakeholdersoftwareprojectSoftwareprojectName(@Param("email") String email,@Param("projectName") String projectName);

    Optional<IDOnlyProjection> findByPersonEmailAndProjectstakeholdersoftwareprojectSoftwareprojectApplicationsExternalId(@Param("email") String email,@Param("externalID") String externalID);

    Optional<IDOnlyProjection> findByInternalIdAndProjectstakeholdersoftwareprojectSoftwareprojectApplicationsExternalId(@Param("internalID")String internalID, @Param("externalID") String externalID);

    Boolean existsByPersonApplicationsExternalIdAndProjectstakeholdersoftwareprojectSoftwareprojectName( @Param("personExternalId") String personExternalId, @Param("projectName") String projectName);

    Boolean existsByPersonApplicationsExternalIdAndProjectstakeholdersoftwareprojectSoftwareprojectApplicationsExternalId(@Param("personExternalId") String personExternalId,@Param("projectExternalId") String projectExternalId);

    Boolean existsByPersonApplicationsExternalIdAndProjectstakeholderartifactArtifactApplicationsExternalId(@Param("personExternalId") String personExternalId,@Param("artifactId") String artifactId);

    Boolean existsByInternalIdAndProjectstakeholderartifactArtifactApplicationsExternalId(@Param("personExternalId") String personExternalId,@Param("artifactId") String artifactId);

    Optional<IDOnlyProjection> findFirstByPersonAndProjectstakeholdersoftwareprojectSoftwareproject(@Param("person") Person person, @Param("project") SoftwareProject project);

    Boolean existsByInternalId(@Param("internalId") String internalId);

    Optional<IDOnlyProjection> findByInternalId(@Param("internalId") String internalId);

    List<IDOnlyProjection> findByPersonEmail(@Param("email") String email);
}
