package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

import  br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcessSpecificProjectProcess;
import org.springframework.data.jpa.repository.JpaRepository;
public interface SpecificProjectProcessSpecificProjectProcessRepository extends JpaRepository<SpecificProjectProcessSpecificProjectProcess, Long>, PagingAndSortingRepository<SpecificProjectProcessSpecificProjectProcess, Long>, ListCrudRepository<SpecificProjectProcessSpecificProjectProcess, Long> {



}
