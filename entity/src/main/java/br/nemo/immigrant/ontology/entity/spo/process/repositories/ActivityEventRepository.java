package br.nemo.immigrant.ontology.entity.spo.process.repositories;

import br.nemo.immigrant.ontology.entity.spo.process.models.ActivityEvent;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.spo.process.models.StateType;
import br.nemo.immigrant.ontology.entity.spo.process.models.SuccessType;

public interface ActivityEventRepository extends JpaRepository<ActivityEvent, Long>, PagingAndSortingRepository<ActivityEvent, Long>, ListCrudRepository<ActivityEvent, Long> {

    Optional<ActivityEvent> findByActivityAndStatetypeAndSuccesstype(Activity activity, StateType statetype, SuccessType successtype);

}
