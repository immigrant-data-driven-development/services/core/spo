package br.nemo.immigrant.ontology.entity.spo.artifact.models;

public enum ArtifactRelation {
    COMPOSED,
    RELATED
}