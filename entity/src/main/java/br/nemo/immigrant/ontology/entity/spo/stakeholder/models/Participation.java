package br.nemo.immigrant.ontology.entity.spo.stakeholder.models;

public enum Participation {
    PARTICIPATESIN,
    ISINCHARGEOF
}