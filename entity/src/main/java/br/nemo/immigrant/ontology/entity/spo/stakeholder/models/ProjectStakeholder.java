package br.nemo.immigrant.ontology.entity.spo.stakeholder.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.spo.project.models.SoftwareProject;

import br.nemo.immigrant.ontology.entity.base.models.CommonConcept;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "projectstakeholder")
public  class ProjectStakeholder  extends CommonConcept  implements Serializable {


  @Id
  protected @GeneratedValue (strategy=GenerationType.IDENTITY)
  Long id;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();


  @OneToMany(mappedBy = "projectstakeholder")
  Set<ProjectStakeholderActivity> projectstakeholderactivity;

  @OneToMany(mappedBy = "projectstakeholder")
  Set<ProjectStakeholderArtifact> projectstakeholderartifact;

  @OneToMany(mappedBy = "projectstakeholder")
  Set<ProjectStakeholderSoftwareProject> projectstakeholdersoftwareproject;


  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ProjectStakeholder elem = (ProjectStakeholder) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ProjectStakeholder {" +
         "id="+this.id+


      '}';
  }
}
